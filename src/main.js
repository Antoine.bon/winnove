import { createApp } from "vue";
import App from "./App.vue";
import { createRouter, createWebHistory } from "vue-router";

const app = createApp(App);

import Dashboard from "./components/Dashboard.vue";
import Profile from "./components/Profile.vue";

const routes = [
  { path: "/", component: Dashboard },
  { path: "/profil", component: Profile },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

app.use(router);

app.mount("#app");
